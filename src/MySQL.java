import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.io.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;


import java.nio.charset.Charset;
import java.nio.charset.UnsupportedCharsetException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.apache.tika.parser.txt.CharsetDetector;
import org.apache.tika.parser.txt.CharsetMatch;

/*** 
https://tika.apache.org/0.8/api/org/apache/tika/parser/txt/CharsetDetector.html 
***/

public class MySQL {
    public static void main(String[] args) {
	println ("Falls Du ein Windows Kommandozeile verwendest, gibst Du 'chcp.com 1254' ein");
	println ("Falls Du CMD.EXE verwendest, w�hlst Du den Font Lucida Console aus");
	Class<?> driver_class=null;
	try {
	    driver_class = Class.forName("com.mysql.cj.jdbc.Driver");
	}

	catch (ClassNotFoundException e) {
	    e.printStackTrace();
	}

	println("Found driver " + driver_class);
	
	Connection conn= null;
	
	String param = "";
	param += "useUnicode=true";
	param += "&useSSL=false";
	param += "&characterEncoding=utf8";
	param += "&characterResultSets=utf8";
	param += "&useUnicode=true&useLegacyDatetimeCode=false&serverTimezone=Turkey";
	/***
	    detectCharsetImpl(b): ISO-8859-9 Latin 5 (Stand 1999)
	***/
	String un = "root";
	String pw = "";

	String URL = "jdbc:mysql://localhost:3306/ausbildungDB";
	URL = URL + "?" + "user=" + un + "&password=" + pw;
	URL = URL + "&" + param;

	try {
	    conn = DriverManager.getConnection(URL);// + "?" + param);
	}
	catch (SQLException e) {
	    
	    e.printStackTrace();
	    return;
	}

	try {
	    println("Verbindung hergestellt zu: " + conn.getMetaData().getURL());
	} catch (SQLException e1) {
	    e1.printStackTrace();
	}
	Statement statement=null;
	
	String query="SELECT id, isim FROM ogrenci;";
	query = "SELECT id, isim FROM ogrenci ORDER BY id LIMIT 30";
	
	try {
	    FileOutputStream fileStream = new FileOutputStream(new File("geschrieben.txt"));
	    OutputStreamWriter schreib = new OutputStreamWriter(fileStream, "UTF-8");
	    System.setOut(new PrintStream(System.out, true, "windows-1254"));
	    /*
	    PrintStream out = new PrintStream("Hello.txt", "UTF-8");
	    System.setOut(out);
	    System.out.println("Hello world!");
	    System.out.println("\u4e16\u754c\u4f60\u597d\uff01");
	    out.close();
	    */
	    //now UTF-8 characters are appearing properly in the ouput
	    
	    try {
		statement = conn.createStatement();
		statement.execute(query);
		ResultSet rs = statement.getResultSet();
		
		String arr = null;
		
		System.out.println("ID  �sim");
		String id = null;
		String name = null;
		
		byte[] defekt = null; 
		String ordentlich = null;
		
		while (rs.next()){
		    print("Zeile: " + rs.getRow());
		    id = rs.getString("id");
		    print(" ID " + id);
		    name = rs.getString("isim");
		    
		    defekt = name.getBytes("ISO-8859-9"); // "Cp1252" ist auch m�glich
		    ordentlich = new String(defekt,"utf-8"); // CHCP.COM 1254 (Statt 857)
		    println(" isim " + name);
		    
		    arr += " " + rs.getString("isim");
		    schreib.write(name + " , "); // Schreibt defekt wenn keiner Charaktercode benutzt wird
	  	}
		
		rs.close();
		schreib.close();

		byte[] b = arr.getBytes();
		try {
		    Charset cs = detectCharsetImpl(b);
		    print("detectCharsetImpl(b): " + cs);
		}
		catch (Exception e) {
		    println("detectCharsetImpl Problem!");
		}
		
	    }
	    catch(SQLException e) {
		println("SQLException Problem!");
		e.printStackTrace();
	    }    
	}
	catch (IOException e){
	    println("IO Problem!");
	    System.err.println("Error: " + e.getMessage());
	}
	
	if (statement != null)
	    {
		try {
		    statement.close();
		}
		catch (SQLException e) {
		    println("SQLException Problem!");
		    e.printStackTrace();
		}
		}
	if (conn != null)
	    {
		try {
		    conn.close();
		}
		catch (SQLException e) {
		    println("SQLException Problem!");
		    e.printStackTrace();
		    }
	    }
    }    
    
    protected static Charset detectCharsetImpl(byte[] buffer) throws Exception {
    	int threshold = 15;
    	Log logger = LogFactory.getLog(MySQL.class);
        
    	CharsetDetector detector = new CharsetDetector();
    	detector.setText(buffer);
    	CharsetMatch match = detector.detect();

    	if(match != null && match.getConfidence() > threshold) {
    		try {
    			println("Charset Zur�ckgeben ist OK. Zur Kontrolle: A�r� da��ndan �stanbul'a");
    			return Charset.forName(match.getName());
		/*  detectCharsetImpl(b): ISO-8859-9 */
		
    		} catch(UnsupportedCharsetException e) {
    			println("Charset Zur�ckgeben ist N�CHT OK!");
    			logger.info("Charset detected as " + match.getName() + " but no JVM support,  skip");
    			}
    	}
    	
    	else {
    		println("Charset konnte nich zur�ckgegeben werden!");
    		return null;
    	}
    	return null;
    }
   
    public static void println(Object line) {
	System.out.println(line);
    }
    public static void print(Object line) {
    System.out.print(line);
    }
}

/***

git@gitlab.com:wissenserwerber/jdbc-test.git (Falsch gegeben. Nicht zum Nutzen)

https://gitlab.com/wissenserwerber/jdbc-test.git
***/

/***
  SELECT default_character_set_name FROM information_schema.SCHEMATA WHERE schema_name = "schemaname";
 
  ==> Hat "empty set" zur�ckgegeben
  
  Bei der JDBC Verbindung der richtige encoding Parameter soll �bergeben werden.
  Im param UTF-8, UTF8 und latin1 werden probiert aber das hat zum Probleml�sen nichts ver�ndert  
  Der param wurde entfernt aber das gleiche Problem. 
  In allen Versuchen kommen die T�rkische Charakters noch defekt auf die gleiche Weise, 
  d.H. die defekten Charakters ver�ndern sich nicht.
***/